const StarshipCard = function({ starship })
{
    return (
        <div className="starship-card">
            <h3 className="starship-name">{starship.name}</h3>
        </div>
    );
};


export default StarshipCard;
