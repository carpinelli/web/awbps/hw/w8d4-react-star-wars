const getAllStarships = async function()
{
    const httpResponse = await fetch("https://swapi.dev/api/starships/");
    const starshipData = await httpResponse.json();
    return starshipData.results;
};


export default getAllStarships;
