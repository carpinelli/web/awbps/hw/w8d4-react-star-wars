import { useState, useEffect } from 'react'

import getAllStarships from './services/sw-api';
import StarshipCard from './components/StarshipCard';
import './App.css'


const App = function()
{
  let jsx = <></>;
  const [starships, setStarships] = useState(null);

  useEffect(() =>
  {
    const setStarshipsAsync = async function()
    {
      const nextStarships = await getAllStarships();
      setStarships((_) => nextStarships)
    };
    setStarshipsAsync();
  }, []);

  if (starships && starships[0].name)
  {
    jsx = (
      <article className="App">
        {starships.map(starship =>
          {
            return <StarshipCard key={starship.name} starship={starship} />
          })}
      </article>
    );
  }
  else
  {
    jsx = <><p>Loading...</p></>
  }

  return jsx;
};


export default App;
